# Example 1: Hello World! (thanks to Mauricio Vergara Ereche)
Name: helloworld
Version: 1.1
Release: 1
Summary: An application that prints “Hello World!”
License: GPL
Group: System Environment/Base
Source: helloworld.tar.gz
BuildRoot: %{_sourcedir}
BuildArch: noarch
%description
This program prints hello world on the screen to avoid the “programmers curse”. The
Programmmers Curse states that unless your first example is “Hello World!”, then
you will be cursed, and never able to use that tool.
%prep

%build

#rozbaleni celeho zabaleneho git projektu
tar -zxvf %{_sourcedir}/helloworld.tar.gz

%install

rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/bin
cp ./helloworld/helloworld.sh $RPM_BUILD_ROOT/usr/bin/helloworld
%clean

rm -rf $RPM_BUILD_ROOT
%files
%defattr(-,root,root)
%attr(0755,root,root) /usr/bin/helloworld
%changelog
* Sun May 14 2006 Tom “spot” Callaway < tcallawa@redhat.com> 1.1-1
- initial package for Red Hat Summit Presentation